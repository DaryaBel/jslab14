export class Person {
    public id: number;
    public name: string;
    public surname: string;
    public phone: number;
    constructor(id:number, name:string, surname:string, phone: number){
        this.id=id;
        this.name=name;
        this.surname=surname;
        this.phone=phone;
    }
}